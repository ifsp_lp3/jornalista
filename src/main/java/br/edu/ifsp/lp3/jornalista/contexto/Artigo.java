package br.edu.ifsp.lp3.jornalista.contexto;

public class Artigo {
	private String titulo;
	private String subtitulo;
	private String texto;
	
	public Artigo() {
	}
	
	public Artigo(String titulo, String subtitulo, String texto) {
		this.titulo = titulo;
		this.subtitulo = subtitulo;
		this.texto = texto;
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getSubtitulo() {
		return subtitulo;
	}
	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
}
