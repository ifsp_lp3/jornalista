package br.edu.ifsp.lp3.jornalista.gerador.html;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.rythmengine.Rythm;
import org.rythmengine.exception.CompileException;

import br.edu.ifsp.lp3.jornalista.contexto.Autor;
import br.edu.ifsp.lp3.jornalista.gerador.IGerador;
import br.edu.ifsp.lp3.jornalista.gerador.TemplateInvalidoException;

public class GeradorHTMLAutor implements IGerador {
	public static final String CAMINHO_TEMPLATE = "templates/autor.html.rythm";

	private Autor autor;

	public GeradorHTMLAutor(Autor autor) {
		this.autor = autor;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	@Override
	public String gerar() throws TemplateInvalidoException {
		String resultado;
		try {
			resultado = Rythm.render(new File(CAMINHO_TEMPLATE), this.autor);
		} catch(CompileException e) {
			throw new TemplateInvalidoException(e);
		}
		
		return resultado;
	}

	@Override
	public void salvarEmArquivo(String caminho) throws IOException, TemplateInvalidoException {
		Writer writer;
		String conteudo;
		
		conteudo = this.gerar();
		
		if (caminho == null || "".equals(caminho)) {
			writer = new FileWriter("./autor.html");
		} else {
			writer = new FileWriter(caminho);
		}

		try {
			writer.write(conteudo);
		} finally {
			writer.close();
		}
	}
}
