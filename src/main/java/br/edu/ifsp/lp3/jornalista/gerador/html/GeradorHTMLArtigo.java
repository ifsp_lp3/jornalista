package br.edu.ifsp.lp3.jornalista.gerador.html;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.rythmengine.Rythm;
import org.rythmengine.exception.CompileException;

import br.edu.ifsp.lp3.jornalista.contexto.Artigo;
import br.edu.ifsp.lp3.jornalista.gerador.IGerador;
import br.edu.ifsp.lp3.jornalista.gerador.TemplateInvalidoException;

public class GeradorHTMLArtigo implements IGerador {
	public static final String CAMINHO_TEMPLATE = "templates/artigo.html.rythm";

	private Artigo artigo;

	public GeradorHTMLArtigo(Artigo artigo) {
		this.artigo = artigo;
	}

	public Artigo getArtigo() {
		return artigo;
	}

	public void setArtigo(Artigo artigo) {
		this.artigo = artigo;
	}

	@Override
	public String gerar() throws TemplateInvalidoException {
		String resultado;
		try {
			resultado = Rythm.render(new File(CAMINHO_TEMPLATE), this.artigo);
		} catch(CompileException e) {
			throw new TemplateInvalidoException(e);
		}
		
		return resultado;
	}

	public void salvarEmArquivo(String caminho) throws IOException, TemplateInvalidoException {
		Writer writer;
		String conteudo;
		
		conteudo = this.gerar();

		if (caminho == null || "".equals(caminho)) {
			writer = new FileWriter("./artigo.html");
		} else {
			writer = new FileWriter(caminho);
		}

		try {
			writer.write(conteudo);
		} finally {
			writer.close();
		}
	}
}
