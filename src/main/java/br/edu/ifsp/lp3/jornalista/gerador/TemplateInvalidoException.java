package br.edu.ifsp.lp3.jornalista.gerador;


public class TemplateInvalidoException extends Exception {
	public TemplateInvalidoException(Throwable cause) {
		super(cause);
	}

	private static final long serialVersionUID = 7256217195172281964L;
}
