package br.edu.ifsp.lp3.jornalista;

import java.util.HashMap;
import java.util.Map;

import org.rythmengine.Rythm;

import org.rythmengine.logger.NullLogger.Factory;
import br.edu.ifsp.lp3.jornalista.ui.Terminal;
import br.edu.ifsp.lp3.jornalista.ui.UI;

public class App {
	public static void main(String[] args) {
		Map<String, Object> conf = new HashMap<>();
		
		conf.put("rythm.log.enabled", false);
		conf.put("rythm.log.factory.impl", Factory.class);
		
		Rythm.init(conf);
		
		// begin the magic
		UI ui = new Terminal();
		ui.iniciar();
	}
}