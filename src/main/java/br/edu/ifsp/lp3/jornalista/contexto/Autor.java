package br.edu.ifsp.lp3.jornalista.contexto;

public class Autor {
	private String nome;
	private String bio;
	
	public Autor() {
	}
	
	public Autor(String nome, String bio) {
		this.nome = nome;
		this.bio = bio;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
}
