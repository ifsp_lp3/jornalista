package br.edu.ifsp.lp3.jornalista.ui;

import java.io.IOException;
import java.util.Scanner;

import br.edu.ifsp.lp3.jornalista.contexto.Artigo;
import br.edu.ifsp.lp3.jornalista.contexto.Autor;
import br.edu.ifsp.lp3.jornalista.gerador.IGerador;
import br.edu.ifsp.lp3.jornalista.gerador.TemplateInvalidoException;
import br.edu.ifsp.lp3.jornalista.gerador.html.GeradorHTMLArtigo;
import br.edu.ifsp.lp3.jornalista.gerador.html.GeradorHTMLAutor;
import br.edu.ifsp.lp3.jornalista.ui.terminal.Menu;
import br.edu.ifsp.lp3.jornalista.ui.terminal.OpcoesMenu;

public class Terminal implements UI {
	Scanner scan;

	@Override
	public void iniciar() {
		scan = new Scanner(System.in);
		Menu menu = new Menu(scan);

		do {
			menu.imprimir();
			menu.lerOpcao();

			switch (menu.getOpcao()) {
			case GERAR_ARTIGO:
				gerarArtigo();
				break;
			case GERAR_AUTOR:
				gerarAutor();
				break;
			case SAIR:
				break;

			default:
				System.out.println("Opção inválida");
				break;
			}
		} while (menu.getOpcao() != OpcoesMenu.SAIR);
	}

	private void gerarArtigo() {
		IGerador gen;
		Artigo artigo = new Artigo();

		System.out.print("Título: ");
		artigo.setTitulo(scan.nextLine());

		System.out.print("Subtítulo: ");
		artigo.setSubtitulo(scan.nextLine());

		System.out.print("Texto: ");
		artigo.setTexto(scan.nextLine());

		String caminho;
		System.out.print("Diretório para salvar o arquivo (padrão: atual) ");
		caminho = scan.nextLine();

		gen = new GeradorHTMLArtigo(artigo);

		boolean ok = false;
		do {
			try {
				gen.salvarEmArquivo(caminho);
				ok = true;
			} catch (IOException e) {
				System.out.println("Não foi possível salvar arquivo: "
						+ e.getMessage());
			} catch (TemplateInvalidoException e) {
				System.out
						.println("Não foi possível salvar arquivo: Template está invalido: "
								+ e.getMessage());
				ok = true;
			}
		} while (!ok);
	}

	private void gerarAutor() {
		IGerador gen;
		Autor autor = new Autor();

		System.out.print("Nome: ");
		autor.setNome(scan.nextLine());

		System.out.print("Biografia: ");
		autor.setBio(scan.nextLine());

		String caminho;
		System.out.print("Diretório para salvar o arquivo (padrão: atual) ");
		caminho = scan.nextLine();

		gen = new GeradorHTMLAutor(autor);

		boolean ok = false;
		do {
			try {
				gen.salvarEmArquivo(caminho);
				ok = true;
			} catch (IOException e) {
				System.out.println("Não foi possível salvar arquivo: "
						+ e.getMessage());
			} catch (TemplateInvalidoException e) {
				System.out
						.println("Não foi possível salvar arquivo: Template está invalido: "
								+ e.getMessage());
				ok = true;
			}
		} while (!ok);

	}
}
