package br.edu.ifsp.lp3.jornalista.ui.terminal;

public enum OpcoesMenu {
	INVALIDO, GERAR_ARTIGO, GERAR_AUTOR, SAIR;

	public static OpcoesMenu valueOf(int ordinal) {
		switch (ordinal) {
		case 1:
			return GERAR_ARTIGO;
		case 2:
			return GERAR_AUTOR;
		case 3:
			return SAIR;
		default:
			return INVALIDO;
		}
	}
}
