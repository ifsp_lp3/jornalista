package br.edu.ifsp.lp3.jornalista.ui.terminal;

import java.util.Scanner;

public class Menu {
	Scanner scan;
	OpcoesMenu opcao;
	
	public Menu(Scanner scanner) {
		scan = scanner;
	}
	
	public void imprimir() {
		System.out.println("");
		System.out.println("========================");
		System.out.println("===    Jornalista    ===");
		System.out.println("========================");
		System.out.println("");
		System.out.println("1) Gerar HTML para Artigo");
		System.out.println("2) Gerar HTML para Autor");
		System.out.println("3) Sair");
		System.out.println("");
		System.out.print("> ");
	}

	public void lerOpcao() {
		try {
			int op = Integer.parseInt(scan.nextLine());
			opcao = (OpcoesMenu.valueOf(op));
		} catch(NumberFormatException e) {
			opcao = OpcoesMenu.INVALIDO;
		}
	}
	
	public OpcoesMenu getOpcao() {
		return opcao;
	}
}
