package br.edu.ifsp.lp3.jornalista.gerador;

import java.io.IOException;

public interface IGerador {
	public String gerar() throws TemplateInvalidoException;
	
	public void salvarEmArquivo(String caminho) throws IOException, TemplateInvalidoException;
}
