Jornalista
==========

Projeto de conclusão da matéria LP3 ([detalhes](http://cmp.ifsp.edu.br/moodle/mod/assign/view.php?id=296)).

## Definição

Gerar páginas HTML a partir de duas coisas: um modelo, único para todas as
páginas, e um objeto específico de cada página contendo dados que a representem.
Esses objetos podem, por exemplo, ter a seguinte estrutura: título, subtítulo,
parágrafo, imagem, legenda. Não vale fazer uma aplicação web; deve-se usar um
template engine isoladamente.

## Pre-requisito

* maven
* java 8

## Uso

A partir do código-fonte:

* `mvn clean package`
* `java -jar target/jornalista-1.0-SNAPSHOT-jar-with-dependencies.jar`

Ou a partir do jar:

* Baixe o jar (por exemplo, a versão 1.0) [aqui](https://bitbucket.org/ifsp_lp3/jornalista/src/90279a6357684aafa7ee2009791567e0ff8af9b2/target/jornalista-1.0-SNAPSHOT-jar-with-dependencies.jar?at=releases).
* Execute `java -jar jornalista-1.0-SNAPSHOT-jar-with-dependencies.jar`

## Autores

Daniel Teixeira
Ricardo Bernardo
